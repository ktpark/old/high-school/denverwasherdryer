//
//  ViewController.swift
//  TableView
//
//  Created by Student on 5/10/16.
//  Copyright © 2016 Student. All rights reserved.
//

import UIKit

class InventoryViewController: UITableViewController {
    
    var products = [Item]()
    
    @IBOutlet weak var add: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Inventory"
        self.navigationItem.rightBarButtonItem = add
        self.navigationItem.leftBarButtonItem = editButtonItem()
        self.refreshControl?.addTarget(self, action: #selector(InventoryViewController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0x5ecae2)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        let inventory = Inventory()
        if let items = inventory {
            products = items.parse()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ListingViewCell", forIndexPath: indexPath) as! ListingViewCell
        
        let product = products[indexPath.row]
        
        cell.productImage.image = product.photo
        cell.productName.text = product.name
        cell.productIdentifier.text = "ID# " + String(product.id)
        cell.productInfo.hidden = false
        cell.productCost.text = "$" + product.costString
        if product.discount != nil {
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "$" + product.discountString)
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
            cell.productInfo.attributedText = attributeString
            cell.productInfo.textColor = UIColor(red: 0.0, green: 0.5, blue: 0.0, alpha: 1.0)
        } else if product.sold == true {
            cell.productInfo.text = "SOLD"
            cell.productInfo.textColor = UIColor.redColor()
        } else {
            cell.productInfo.text = nil
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            let controller = UIAlertController(title: "Enter password to delete:", message: "Enter your employee password to delete #" + String(products[indexPath.row].id), preferredStyle: .Alert)
            
            controller.addTextFieldWithConfigurationHandler({ (textField) -> Void in
                textField.text = "Password"
                textField.secureTextEntry = true
                textField.layer.cornerRadius = 10
            })
            
            controller.addAction(UIAlertAction(title: "Delete", style: .Destructive, handler: {(action: UIAlertAction) in
                if(controller.textFields![0].text == "Password") {
                    self.products.removeAtIndex(indexPath.row)
                    self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                }
            }))
            controller.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
            
            presentViewController(controller, animated: true, completion: nil)
            
            
        } else if editingStyle == .Insert {
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowDetail" {
            let addNewProductViewController = segue.destinationViewController as! AddNewProductController
            if let selectedItemCell = sender as? ListingViewCell {
                let indexPath = tableView.indexPathForCell(selectedItemCell)!
                let selectedItem = products[indexPath.row]
                addNewProductViewController.item = selectedItem
            }
        } else if segue.identifier == "AddItem" {
            print("Adding new meal.")
        }
    }
    
    func handleRefresh(refreshControl: UIRefreshControl) {
        let inventory = Inventory()
        if let items = inventory {
            products = items.parse()
        }
        
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }

}

