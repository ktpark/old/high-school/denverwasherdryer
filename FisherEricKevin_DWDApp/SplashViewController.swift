//
//  SplashViewController.swift
//  FisherEricKevin_DWDApp
//
//  Created by Kevin Park on 5/20/16.
//  Copyright © 2016 Kerchief. All rights reserved.
//

import UIKit

import GoogleAPIClient
import GTMOAuth2
import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    private let kKeychainItemName = "FisherEricKevin_DWDApp"
    private let kClientID = "703345478694-4i26j17rtd2dadbj21pvosmga8a3eml6.apps.googleusercontent.com"
    
    var google = googleStuff()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        // Do any additional setup after loading the view.
        
        if let auth = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(kKeychainItemName, clientID: kClientID, clientSecret: nil) {
            googleStuff.service.authorizer = auth
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Creates the auth controller for authorizing access to Google Calendar API
    private func createAuthController() -> GTMOAuth2ViewControllerTouch {
        let scopeString = "https://www.googleapis.com/auth/calendar"
        return GTMOAuth2ViewControllerTouch(
            scope: scopeString,
            clientID: kClientID,
            clientSecret: nil,
            keychainItemName: kKeychainItemName,
            delegate: self,
            finishedSelector: #selector(SplashViewController.viewController(_:finishedWithAuth:error:))
        )
    }
    
    // Handle completion of the authorization process, and update the Google Calendar API
    // with the new credentials.
    @objc func viewController(vc : UIViewController, finishedWithAuth authResult : GTMOAuth2Authentication, error : NSError?) {
        
        if let error = error {
            googleStuff.service.authorizer = nil
            google.showAlert("Authentication Error", message: error.localizedDescription)
            return
        }
        
        googleStuff.service.authorizer = authResult
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        if let authorizer = googleStuff.service.authorizer,
            canAuth = authorizer.canAuthorize where canAuth {
            google.getAppointments(NSDate())
            print("Can Auth")
        } else {
            presentViewController(createAuthController(), animated: true, completion: nil)
        }
        self.performSegueWithIdentifier("splashSegue", sender: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
