//
//  TestViewCell.swift
//  TableView
//
//  Created by Student on 5/10/16.
//  Copyright © 2016 Student. All rights reserved.
//

import UIKit

class ListingViewCell: UITableViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productIdentifier: UILabel!
    @IBOutlet weak var productCost: UILabel!
    @IBOutlet weak var productInfo: UILabel!
    
}