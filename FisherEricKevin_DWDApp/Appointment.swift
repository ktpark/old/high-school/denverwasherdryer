//
//  Appointment.swift
//  FisherEricKevin_DWDApp
//
//  Created by Student on 5/16/16.
//  Copyright © 2016 Kerchief. All rights reserved.
//

import Foundation

enum Type : String {
    case SERVICE = "SERVICE", INSTALL = "INSTALL", DELIVERY = "DELIVERY", MEETING = "MEETING", REMOVAL = "REMOVAL", QUOTE = "QUOTE", MISC = "MISC"
}

struct Appointment : CustomStringConvertible {
    var type: Type
    var desc: String
    var location: String
    
    var start : NSDate
    var end : NSDate
    
    var time : String {
        get {
            return NSDateFormatter.localizedStringFromDate(start, dateStyle: .ShortStyle, timeStyle: .ShortStyle) + " - " + NSDateFormatter.localizedStringFromDate(end, dateStyle: .NoStyle, timeStyle: .ShortStyle)
        }
        
    }
    
    
    var assocProducts : [Product]
    
    var description: String {
        var ret = ""
        
        ret += String(type)
        ret += ": " + desc
        
//        for a in assocProducts {
//            ret += "#" + String(a.ID)
//        }
        
        return ret
    }
    
//    static func generateNewId() -> String {
//        let date = NSDate()
//        let calendar = NSCalendar.currentCalendar()
//        let components = calendar.components([.Year, .Month, .Day, .Hour, .Minute, .Second, .Nanosecond], fromDate: date)
//        
//        let val = components.year + components.month + components.day + components.hour + components.minute + components.second
//        let value = Int64(val)
//        
//        return String(value)
//    }
    
}