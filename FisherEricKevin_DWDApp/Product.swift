//
//  Product.swift
//  FisherEricKevin_DWDApp
//
//  Created by Student on 5/4/16.
//  Copyright © 2016 Kerchief. All rights reserved.
//

import UIKit

struct Product {
    
    var name: String
    var ID: Int
    var cost: Int
    var image: UIImage
    
    func toPOSTData() -> String {
        return "name=\(name)?ID=\(ID)cost=\(cost)"
    }
    
}