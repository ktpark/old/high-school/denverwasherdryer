//
//  AddNewProductController.swift
//  FisherEricKevin_DWDApp
//
//  Created by Student on 5/19/16.
//  Copyright © 2016 Kerchief. All rights reserved.
//

import UIKit

class AddNewProductController : UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var modelNumberField: UITextField!
    @IBOutlet weak var serialNumberField: UITextField!
    @IBOutlet weak var costField: UITextField!
    @IBOutlet weak var discountAmountField: UITextField!
    @IBOutlet weak var discountSwitch: UISwitch!
    @IBOutlet weak var dateAddedPicker: UIDatePicker!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var productImageView: UIImageView!
    
    var image = UIImage(named: "Camera-Icon.png")
    var imagePicker = UIImagePickerController()
    
    var item: Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Assign Delegates
        nameField.delegate = self
        modelNumberField.delegate = self
        serialNumberField.delegate = self
        costField.delegate = self
        discountAmountField.delegate = self
        imagePicker.delegate = self
        submitButton.layer.cornerRadius = 10
        
        if let item = item {
            nameField.text = item.name
            costField.text = item.costString
            if item.discount != nil {
                discountSwitch.on = true
                discountAmountField.text = item.difference
            }
            //dateAddedPicker.date = NSDateFormatter.dateFromString(item.date)
        }
        
        self.hideKeyboardWhenTappedAround()
        
    }
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        let isPresentingInAddItemMode = presentingViewController is UITabBarController
        if isPresentingInAddItemMode {
            dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            navigationController!.popViewControllerAnimated(true)
        }
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == nameField { // Switch focus to other text field
            modelNumberField.becomeFirstResponder()
        } else if textField == modelNumberField {
            serialNumberField.becomeFirstResponder()
        } else if textField == serialNumberField {
            costField.becomeFirstResponder()
        } else if textField == costField {
            discountAmountField.becomeFirstResponder()
        } else if textField == discountAmountField {
            self.dismissKeyboard()
        }
        return false
    }
    
    @IBAction func imageTapped(sender: UITapGestureRecognizer) {
        print("Tapped")
        if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .Camera
            imagePicker.cameraCaptureMode = .Photo
            imagePicker.modalPresentationStyle = .FullScreen
        
            imagePicker.popoverPresentationController?.sourceView = productImageView
        
            presentViewController(imagePicker, animated: true, completion: nil)
        } else {
            noCamera()
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        productImageView.contentMode = .ScaleAspectFit
        productImageView.image = chosenImage
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(title: "No Camera", message: "Sorry, this device has no camera. Would you like to pick an image from your library instead?", preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "Ok", style:.Default, handler: {(action: UIAlertAction) in
            self.photoFromLibrary()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertVC.addAction(cancelAction)
        alertVC.addAction(okAction)
        
        presentViewController(alertVC, animated: true, completion: nil)
    }
    
    func photoFromLibrary() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        imagePicker.modalPresentationStyle = .Popover
        imagePicker.popoverPresentationController?.sourceView = productImageView
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func submitPressed(sender: AnyObject) {
        
    }
    
    
    
    
    
}
