//
//  AppointmentViewCell.swift
//  Appointments
//
//  Created by Student on 5/20/16.
//  Copyright © 2016 Eric. All rights reserved.
//

import UIKit

class AppointmentViewCell: UITableViewCell {
    
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var address: UILabel!

}
