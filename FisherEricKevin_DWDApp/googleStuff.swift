//
//  ViewController.swift
//  QuickstartApp
//
//  Created by Student on 10/05/2016.
//  Copyright © 2016 com.example. All rights reserved.
//

import GoogleAPIClient
import GTMOAuth2
import UIKit

class googleStuff : NSObject {
    
    private let kKeychainItemName = "FisherEricKevin_DWDApp"
    private let kClientID = "703345478694-18s84efqli55vnucst8bmgmbplb36nbl.apps.googleusercontent.com"
    
    // If modifying these scopes, delete your previously saved credentials by
    // resetting the iOS simulator or uninstall the app.
    private let scopes = [kGTLAuthScopeCalendarReadonly]
    
    static let service = GTLServiceCalendar()
    
    
    let output = UITextView()
    
    static var apps = [Appointment]()
    
    // When the view loads, create necessary subviews
    // and initialize the Google Calendar API service
    
    // When the view appears, ensure that the Google Calendar API service is authorized
    // and perform API calls
    
    
    // Construct a query and get a list of upcoming events from the user calendar
    func fetchEvents() {
        //Normal stuff
        
        let query = GTLQueryCalendar.queryForEventsListWithCalendarId("primary")
        query.maxResults = 50
        query.timeMin = GTLDateTime(date: NSDate(), timeZone: NSTimeZone.localTimeZone())
        query.singleEvents = true
        query.orderBy = kGTLCalendarOrderByStartTime
        googleStuff.service.executeQuery(
            query,
            delegate: self,
            didFinishSelector: #selector(self.displayResultWithTicket(_:finishedWithObject:error:))
        )
    }
    
    func getAppointments(afterDate: NSDate) {
        
        let query = GTLQueryCalendar.queryForEventsListWithCalendarId("primary")
        query.maxResults = 10
        query.timeMin = GTLDateTime(date: afterDate, timeZone: NSTimeZone.localTimeZone())
        query.singleEvents = true
        query.orderBy = kGTLCalendarOrderByStartTime
        
        googleStuff.service.executeQuery(query, delegate: self, didFinishSelector: #selector(self.createAppointments(_:finishedWithObject:error:)))
    }
    
    @objc func createAppointments(ticket: GTLServiceTicket, finishedWithObject response : GTLCalendarEvents, error : NSError?) {
        if let error = error {
            showAlert("Error", message: error.localizedDescription)
            return
        }
        
        var appointments = [Appointment]()
        
        if let events = response.items() where !events.isEmpty {
            for event in events as! [GTLCalendarEvent] {
                
                let start : GTLDateTime! = event.start.dateTime ?? event.start.date
                let end : GTLDateTime! = event.end.dateTime ?? event.end.date
                
                var typeArr = event.summary.componentsSeparatedByString(":")
                print(typeArr)
                var type = typeArr[0]
                var desc = typeArr[1].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                
                var app = Appointment(type: Type(rawValue: type)!, desc: desc, location: event.location, start: start.date, end: end.date, assocProducts: [])
                
                appointments.append(app)
            }
        } else {
            print("No appointments")
        }
        
        googleStuff.apps.appendContentsOf(appointments)
        
    }
    
    // Display the start dates and event summaries in the UITextView
    @objc func displayResultWithTicket( ticket: GTLServiceTicket, finishedWithObject response : GTLCalendarEvents, error : NSError?) {
        
        if let error = error {
            showAlert("Error", message: error.localizedDescription)
            return
        }
        
        
        
        var eventString = ""
        
        if let events = response.items() where !events.isEmpty {
            for event in events as! [GTLCalendarEvent] {
                let start : GTLDateTime! = event.start.dateTime ?? event.start.date
                let startString = NSDateFormatter.localizedStringFromDate(
                    start.date,
                    dateStyle: .ShortStyle,
                    timeStyle: .ShortStyle
                )
                eventString += "\(startString) - \(event.summary)\n"
            }
        } else {
            eventString = "No upcoming events found."
        }
        
        output.text = eventString
    }
    
    
    // Creates the auth controller for authorizing access to Google Calendar API
    private func createAuthController() -> GTMOAuth2ViewControllerTouch {
        let scopeString = "https://www.googleapis.com/auth/calendar"
        return GTMOAuth2ViewControllerTouch(
            scope: scopeString,
            clientID: kClientID,
            clientSecret: nil,
            keychainItemName: kKeychainItemName,
            delegate: self,
            finishedSelector: #selector(googleStuff.viewController(_:finishedWithAuth:error:))
        )
    }
    
    // Handle completion of the authorization process, and update the Google Calendar API
    // with the new credentials.
    @objc func viewController(vc : UIViewController, finishedWithAuth authResult : GTMOAuth2Authentication, error : NSError?) {
        
        if let error = error {
            googleStuff.service.authorizer = nil
            showAlert("Authentication Error", message: error.localizedDescription)
            return
        }
        
        googleStuff.service.authorizer = authResult
    }
    
    // Helper for showing an alert
    func showAlert(title : String, message: String) {
        let alert = UIAlertView(
            title: title,
            message: message,
            delegate: nil,
            cancelButtonTitle: "OK"
        )
        alert.show()
    }
    
    
    
    //Adds a new event to the calendar
    func addNewEvent(summary: String, location: String, startTime: NSDate, endTime: NSDate) {
        
        print("Event add started")
        
        let event = GTLCalendarEvent()
        
        //Set the summary
        event.summary = summary
        
        //Get and correctly set the start and end times
        let startEventTime = GTLCalendarEventDateTime()
        startEventTime.dateTime = GTLDateTime(date: startTime, timeZone: NSCalendar.currentCalendar().timeZone)
        
        let endEventTime = GTLCalendarEventDateTime()
        endEventTime.dateTime = GTLDateTime(date: endTime, timeZone: NSCalendar.currentCalendar().timeZone)
        
        startEventTime.timeZone = "-07:00"
        endEventTime.timeZone = "-07:00"

        //Add the start and end times
        event.start = startEventTime
        event.end = endEventTime
        
        //Add new location
        event.location = location
        
        //Create the query
        let query = GTLQueryCalendar.queryForEventsInsertWithObject(event, calendarId: "primary")
        
        //Execute the query
        googleStuff.service.executeQuery(query, delegate: self, didFinishSelector: #selector(googleStuff.addedEvent(_:finishedWithObject:error:)))
        
    }
    
    
    
    func addedEvent(ticket: GTLServiceTicket, finishedWithObject response : GTLCalendarEvents, error : NSError?) {
//        print(ticket.APIKey)
        if let err = error {
            showAlert("Error: ", message: error.debugDescription)
        }
        
        print("No error")
        
    }
    
}
