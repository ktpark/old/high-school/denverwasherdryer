
//
//  File.swift
//  FisherEricKevin_DWDApp
//
//  Created by Student on 5/18/16.
//  Copyright © 2016 Kerchief. All rights reserved.
//

import UIKit
import Foundation


class AddNewAppointmentController : UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var appointmentTypeButton: UIButton!
    
    @IBOutlet weak var location: UITextField!
    
    @IBOutlet weak var desc: UITextView!
    
    @IBOutlet weak var startDateTime: UIDatePicker!
    @IBOutlet weak var endDateTime: UIDatePicker!
    
    
    
    var appointmentType : Type! = Type.MISC
    
     var google = googleStuff()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startDateTime.minimumDate = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .ShortStyle
        dateFormatter.timeStyle = .ShortStyle
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        
        let date = NSDate()
        let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let components = cal.components([.Day , .Month, .Year, .Hour, .Minute ], fromDate: date)
        components.hour = 1
        components.minute = 0
        let newDate = cal.dateFromComponents(components)
        endDateTime.date = newDate!
        
        location.delegate = self
        
        desc.layer.borderWidth = 1
        desc.layer.borderColor = CGColor.colorWithHex(0xc5c5c5)
        desc.layer.cornerRadius = 10
        
        self.hideKeyboardWhenTappedAround()
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == location { // Switch focus to other text field
            desc.becomeFirstResponder()
        }
        return false
    }
    
    @IBAction func tappedAppointmentType(sender: AnyObject) {
        
        let alertController = UIAlertController(title: "Appointment Type", message: nil, preferredStyle: .ActionSheet)
        
        
        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
            print("Is iPad")
            alertController.modalPresentationStyle = .Popover
            alertController.popoverPresentationController?.sourceView = appointmentTypeButton
        }
        
        alertController.addAction(UIAlertAction(title: "MISC" , style: .Default, handler: {(action: UIAlertAction) in
                self.appointmentType = Type.MISC
                self.appointmentTypeButton.setTitle(String(Type.MISC), forState: .Normal)
        }))
        alertController.addAction(UIAlertAction(title: "SERVICE" , style: .Default, handler: {(action: UIAlertAction) in
            self.appointmentType = Type.SERVICE
            self.appointmentTypeButton.setTitle(String(Type.SERVICE), forState: .Normal)
        }))
        alertController.addAction(UIAlertAction(title: "INSTALL" , style: .Default, handler: {(action: UIAlertAction) in
            self.appointmentType = Type.INSTALL
            self.appointmentTypeButton.setTitle(String(Type.INSTALL), forState: .Normal)
        }))
        alertController.addAction(UIAlertAction(title: "DELIVERY" , style: .Default, handler: {(action: UIAlertAction) in
            self.appointmentType = Type.DELIVERY
            self.appointmentTypeButton.setTitle(String(Type.DELIVERY), forState: .Normal)
        }))
        alertController.addAction(UIAlertAction(title: "MEETING" , style: .Default, handler: {(action: UIAlertAction) in
            self.appointmentType = Type.MEETING
            self.appointmentTypeButton.setTitle(String(Type.MEETING), forState: .Normal)
        }))
        alertController.addAction(UIAlertAction(title: "REMOVAL" , style: .Default, handler: {(action: UIAlertAction) in
            self.appointmentType = Type.REMOVAL
            self.appointmentTypeButton.setTitle(String(Type.REMOVAL), forState: .Normal)
        }))
        alertController.addAction(UIAlertAction(title: "QUOTE" , style: .Default, handler: {(action: UIAlertAction) in
            self.appointmentType = Type.QUOTE
            self.appointmentTypeButton.setTitle(String(Type.QUOTE), forState: .Normal)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel" , style: .Cancel, handler: nil))
        
        
        self.presentViewController(alertController, animated: true, completion: nil)
        //showViewController(alertController, sender: self)
        
    }
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        let isPresentingInAddEventMode = presentingViewController is UITabBarController
        if isPresentingInAddEventMode {
            dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            navigationController!.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func submitTapped(sender: AnyObject) {
        
        var message = String(appointmentType!) + ": " + desc.text
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .ShortStyle
        dateFormatter.timeStyle = .ShortStyle
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        
        //let cal = NSCalendar.dateByAddingUnit(NSCalendar.).
        
        let cal = NSCalendar.currentCalendar()
        let compEnd = cal.components([.Hour, .Minute], fromDate: endDateTime.date)
        
        let endDate = cal.dateByAddingComponents(compEnd, toDate: startDateTime.date, options: [])
        print(dateFormatter.stringFromDate(endDate!))
        
        //let comp = cal.
        
        
        //let endDate = cal.dateByAddingUnit(.Hour, vaue)
        
        //print(dateFormatter.stringFromDate(startDateTime.date) + " " + dateFormatter.stringFromDate(endDate))
        
        let dispMessage = message + ", from " + dateFormatter.stringFromDate(startDateTime.date) + " to " + dateFormatter.stringFromDate(endDate!)
        
        let alertController = UIAlertController(title: "Confirm Appointment", message: dispMessage, preferredStyle: .Alert)
        
        alertController.popoverPresentationController?.sourceView = appointmentTypeButton
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
            self.google.addNewEvent(message, location: self.location.text!, startTime: self.startDateTime.date, endTime: endDate!)
            self.dismissViewControllerAnimated(true, completion: nil)
            print("Event add call")
        }))
        
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
}

extension UIColor {
    
    convenience init(hex: Int) {
        
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
        
    }
    
}

extension CGColor {
    
    class func colorWithHex(hex: Int) -> CGColorRef {
        
        return UIColor(hex: hex).CGColor
        
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

