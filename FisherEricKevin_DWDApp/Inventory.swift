//
//  Inventory.swift
//  Inventory
//
//  Created by Kevin Park on 5/10/16.
//  Copyright © 2016 TekHak. All rights reserved.
//

import UIKit

class Item {
    var photo: UIImage
    var name: String
    var sold: Bool
    var cost: Float
    var discount: Float?
    var date: String
    var id: Int
    
    var costString: String {
        get {
            return NSString(format: "%.2f", cost) as String
        }
    }
    
    var discountString: String {
        get {
            return NSString(format: "%.2f", discount!) as String
        }
    }
    
    var difference: String {
        get {
            return NSString(format: "%.2f", discount! - cost) as String
        }
    }
    
    init(photo: UIImage, name: String, sold: Bool, cost: Float, discount: Float?, date: String, id: Int) {
        self.photo = photo
        self.name = name
        self.sold = sold
        self.cost = cost
        self.discount = discount
        self.date = date
        self.id = id
    }
    
}

class Inventory {
    
    var site: String
    
    init?()
    {
        let myURLString = "http://www.denverwasherdryer.com/inventory/"
        
        if let myURL = NSURL(string: myURLString) {
            
            do {
                let fullSite = try String(contentsOfURL: myURL, encoding: NSUTF8StringEncoding)
                let firstIndex = fullSite.rangeOfString("<div class=\"inventory_item_holder\">", options: .LiteralSearch, range: nil, locale: nil)?.startIndex ?? fullSite.startIndex
                let lastIndex = fullSite.rangeOfString("<div class=\"navigation\">", options: .LiteralSearch, range: nil, locale: nil)?.startIndex ?? fullSite.endIndex
                site = fullSite.substringWithRange(firstIndex ..< lastIndex)
                
            } catch {
                print("Error : \(error)")
                return nil
            }
        } else {
            print("Error: \(myURLString) doesn't  URL")
            return nil
        }
    }
    
    func parse() -> [Item] {
    
        var items = [Item]()
        
        while let position = site.rangeOfString("<div class=\"inventory_item_image\">") {
            site = site.substringFromIndex(position.endIndex)
            
            var photoLink = extract(site, leftBound: "src=\"", rightBound: "\" alt=")
            if photoLink.rangeOfString("http") == nil {
                photoLink = "http://www.denverwasherdryer.com" + photoLink
            }
            let photoURL = NSURL(string: photoLink)
            let photo: UIImage = UIImage(data: NSData(contentsOfURL: photoURL!)!)!
            
            let title = extract(site, leftBound: "<h2>", rightBound: "</h2>")
            let sold = extract(site, leftBound: "<span", rightBound: "</span>").rangeOfString("SOLD") != nil ? true : false
            var cost = extract(site, leftBound: "<span>", rightBound: " <span ")
            var original: Float?
            if let sale = cost.rangeOfString("SALE Price: $") {
                cost = cost.substringFromIndex(sale.endIndex)
                original = Float(extract(site, leftBound: "Original Price: $", rightBound: "</span><span>"))!
            } else {
                let dollarSign = cost.rangeOfString("$")
                cost = cost.substringFromIndex((dollarSign?.endIndex)!)
            }
            let date = extract(site, leftBound: "</span></span><span>", rightBound: "</span><span>ID")
            let id = Int(extract(site, leftBound: "</span><span>ID# ", rightBound: "<a"))!
            /*if let originalCost = original {
                print("Original: \(originalCost)")
             }*/
            items += [Item(photo: photo, name: title, sold: sold, cost: Float(cost)!, discount: original, date: date, id: id)]
        }
        
        return items
    }
    
    func extract(let html: String, let leftBound: String, let rightBound: String) -> String {
        // Extracting needed information from html code
        let firstBound = html.rangeOfString(leftBound, options: .LiteralSearch, range: nil, locale: nil)?.endIndex ?? html.startIndex
        let secondBound = html.rangeOfString(rightBound, options: .LiteralSearch, range: nil, locale: nil)?.startIndex ?? html.endIndex
        let data = html.substringWithRange(firstBound ..< secondBound)
        // Decode html entities
        return String(htmlEncodedString: data)
    }
    
}

extension String {
    init(htmlEncodedString: String) {
        if let encodedData = htmlEncodedString.dataUsingEncoding(NSUTF8StringEncoding){
            let attributedOptions : [String: AnyObject] = [
                NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding
            ]
            
            do{
                if let attributedString:NSAttributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil){
                    self.init(attributedString.string)
                }else{
                    print("error")
                    self.init(htmlEncodedString)     //Returning actual string if there is an error
                }
            }catch{
                print("error: \(error)")
                self.init(htmlEncodedString)     //Returning actual string if there is an error
            }
        }else{
            self.init(htmlEncodedString)     //Returning actual string if there is an error
        }
    }
}
