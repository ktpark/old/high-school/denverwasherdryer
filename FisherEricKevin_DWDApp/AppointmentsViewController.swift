//
//  ViewController.swift
//  Appointments
//
//  Created by Student on 5/19/16.
//  Copyright © 2016 Eric. All rights reserved.
//

import UIKit

class AppointmentsViewController: UITableViewController {
    
    let threshold = 100.0 // threshold from bottom of tableView
    var isLoadingMore = false // flag
    let google = googleStuff()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.parentViewController!.title = "Appointments"
        //appointments += [Appointment(type: "Repair", date: "5/12/16", address: "5633 Southmoor Cir CO 80113"), Appointment(type: "Install", date: "5/14/16", address: "3111 Cherryridge Rd CO 80113"), Appointment(type: "Repair", date: "5/25/16", address: "5448 DTC Pkway CO 80111")]
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0x5ecae2)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return googleStuff.apps.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AppointmentViewCell", forIndexPath: indexPath) as! AppointmentViewCell
        
        let appointment = googleStuff.apps[indexPath.row]
        
        cell.type.text = String(appointment.type)
        cell.date.text = appointment.time
        cell.address.text = appointment.location
        
        return cell
    }
    
    @IBAction func unwindToAppointmentList(sender: UIStoryboardSegue) {
        if sender.sourceViewController is AppointmentsViewController {
            self.tableView.reloadData()
        }
    }
    
//    override func scrollViewDidScroll(scrollView: UIScrollView) {
//        let contentOffset = scrollView.contentOffset.y
//        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
//        
//        if !isLoadingMore && (Float(maximumOffset) - Float(contentOffset) <= Float(threshold)) {
//            // Get more data - API call
//            self.isLoadingMore = true
//            
//            if(appointments.count == 0) {
//                google.getAppointments(NSDate())
//            } else {
//                google.getAppointments(appointments[appointments.count - 1].start)
//            }
//            
//            appointments = googleStuff.
//            // Update UI
//            dispatch_async(dispatch_get_main_queue()) {
//                self.tableView.reloadData()
//                self.isLoadingMore = false
//            }
//        }
//    }

}

